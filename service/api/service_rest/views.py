from django.shortcuts import render
from .models import Technician, Appointment
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
import json


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_id",
        "first_name",
        "last_name",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician"
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician"
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder = TechnicianListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder = TechnicianListEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {"error": "Method Not allowed"},
            status=405
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_technician_details(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician Does not exist"}
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse(
                {"error": "Invalid JSON in request body"},
                status=400
            )
        if not isinstance(content, dict):
            return JsonResponse(
                {"error": "Invalid data format in request body"},
                status=400
            )

    
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder = TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        technician = get_object_or_404(Technician, id=pk)

        count, _ = technician.delete()

        if count > 0:
            return JsonResponse({"deleted": True})
        else:
            return JsonResponse({"error": "No technician found for deletion."}, status=404)

    else:
        return JsonResponse(
            {"error": "Method Not allowed"},
            status=405
        )



@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder = AppointmentListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Employee Id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder = AppointmentListEncoder,
            safe=False,
        )
    
    else:
        return JsonResponse(
            {"error": "Method Not allowed"},
            status=405
        )
    

@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment_details(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                {"appointment": appointment},
                encoder = AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Appointment does not exist"},
                status=404
            )
    elif request.method == "DELETE":
        appointment = get_object_or_404(Appointment, id=pk)
        count, _ = appointment.delete()

        if count > 0:
            return JsonResponse({"deleted": True})
        else:
             return JsonResponse({"error": "No appointment found for deletion."}, status=404)
    else:
        return JsonResponse(
            {"error": "Method Not allowed"},
            status=405
        )




@require_http_methods(["PUT"])
def api_appointment_cancel(request, pk):
    if request.method == "PUT":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse(
                {"error": "Invalid data format in request body"},
                status=400
            )
        try:
            appointment = Appointment.objects.get(id=pk)
            if "technician" in content:
                try:
                    technician = Technician.objects.get(id=content["technician"])
                    content["technician"] = technician
                except Technician.DoesNotExist:
                    return JsonResponse(
                        {"message": "Invalid Employee Id"},
                        status=400
                )
            if "status" in content:
                allowed_statuses = ["cancelled"]
                if content["status"] not in allowed_statuses:
                    return JsonResponse(
                        {"error": f"Invalid status. Allowed values are: {', '.join(allowed_statuses)}"},
                        status=400
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Appointment does not exist"},
                status=404
            )
        Appointment.objects.filter(id=pk).update(**content)
        return JsonResponse(
            {"appointment": appointment},
            encoder = AppointmentDetailEncoder,
            safe=False,
        )


    else:
        return JsonResponse(
            {"error": "Method Not allowed"},
            status=405
        )

@require_http_methods(["PUT"])
def api_appointment_finish(request, pk):
    if request.method == "PUT":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse(
                {"error": "Invalid data format in request body"},
                status=400
            )
        try:
            appointment = Appointment.objects.get(id=pk)
            if "technician" in content:
                try:
                    technician = Technician.objects.get(id=content["technician"])
                    content["technician"] = technician
                except Technician.DoesNotExist:
                    return JsonResponse(
                        {"message": "Invalid Employee Id"},
                        status=400
                )
            if "status" in content:
                allowed_statuses = ["finished"]
                if content["status"] not in allowed_statuses:
                    return JsonResponse(
                        {"error": f"Invalid status. Allowed values are: {', '.join(allowed_statuses)}"},
                        status=400
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Appointment does not exist"},
                status=404
            )
        Appointment.objects.filter(id=pk).update(**content)
        return JsonResponse(
            {"appointment": appointment},
            encoder = AppointmentDetailEncoder,
            safe=False,
        )


    else:
        return JsonResponse(
            {"error": "Method Not allowed"},
            status=405
        )