from django.db import models
from django.urls import reverse

# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=10)

    def __str__(self):
        return "{} {}".format(self.last_name, self.first_name)
    
    def get_api_url(self):
        return reverse("api_list_appointments", kwargs={"pk": self.pk})
    
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    sold = models.CharField(max_length=100)

class Appointment(models.Model):
    STATUS_CHOICES = [
        ('created', 'Created'),
        ('canceled', 'Canceled'),
        ('finished', 'Finished'),
    ]
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=255)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default='created')
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name = "technician",
        on_delete = models.CASCADE,
    )

    def __str__(self):
         return f"Appointment at {self.date_time} for {self.reason} with {self.technician}."


    def get_api_url(self):
        return reverse("api_list_service", kwargs={"pk": self.pk})