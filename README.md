# AutoFlow

Team:

* Hector Reyes - Service microservice
* Brian Stormes - Sales microservice

## Design

![Alt text](<images/Project bravo diagram.png>)


## Startup Instruction Guide
In order to get the application up and running, 3 commands are required in this order:
docker volume create beta-data
docker-compose build
docker-compose up
## Service microservice

The Service microservice contains 3 models: Technician, Appointment, and AutomobileVO. The application utilizes react for the front-end of the application which utilizes these models in a variety of ways.Originating from the technician model, the application allows the ability to view the list of technicians as well as add a new technician. Originating from the appointment model, the application allows the ability to view the list of active service appointments (those that are not cancelled or finished), view service history list of all service appointments, as well as create a new service appointment. The AutomobileVO model is a value object, referencing only the 'vin' and 'sold' properties fron the parent model. The automobileVO is utilized on the backend to compare incoming automobile data what the data stored in the automobileVo as a cross reference.

## Sales microservice

My plan is to work through the back end first getting my models complete so that i can create views based off of those models. Then i will create my urls that will be based of my views. this will ensure that each component commincates as I want them and solid paths are established.

Once the backend code is written I will systematically create and test each API before moving to the next.

The API's being tested will ensure that everything should work as i begin working on the front end.

The front end will be completed last after the backend is completed and working. I will use react to complete the front end.

The backend has 4 models one of which is a VO to the automobile model within Inventory. This allows the Sales to poll for vins and sold status. The VO also allows the creation of auto sales based off of unsold vehicles which the sales _list api checks for.

The salesperson model is used within the sales api's and salesperson api's in order to get, post, and delete data. The salesperson is used within the sales api's to establish the salesperson connected to an auto sale.

The customer model like the salesperson is used within its own api's (list_customers and show_customers) as well as the sales api where it is used to set a purchasing customer.

Lastly the Sales api's uses each of the previous models as foriegn keys as well as price to set a sale instance or delete it.

All list api's already have update functionality implemented within them.

The frontend was created using react.

The saleslist acts as a main page for sales and populates the list of all sales. Within this page are two links nested within the saleslist for easier movement to the create sale, or sale person history. Click on the new sale button and you'll instantly see the create sale form in which a new sale can be created and submitted and will automatically clear once submitted. The sales main button will take you back to the sales list page where you can then navigate to the sales person history page which also has a link back to sales main. You will see that delete functionality does not exist on the history page and this is to limit this functionality to one endpoint.

The SalespeopleList page is a list of all sales employees and acts as a home page for salespeople housing a link to the salesperson creation form. The sales person creation form much like the sale creation form will reset automatically once you submit a valid entry for a new sales employee.

The CustomerList page is a list of all existing customers and acts as a home page for customers and houses a link to the customer creation form. The customer creation form much like the sale creation form will reset automatically once you submit a valid entry for a new customer.

Lastly, The ManufacturerList page is a list of all existing manufacturers and acts as a home page for Manufacturers and houses a link to the Manufacturer creation form. The manufacturer creation form much like the sale creation form will reset automatically once you submit a valid entry for a manufacturer.

The models list populates a list of autos that are within the inventory (unsold). The functionality for a link to the models form is within the code and staged for integration with the models form but is commented out as the models form was created by my partner. (I will try to have this integrated prior to project submission time.)

Within the lengthy Nav Bar are 18 links which will take you to each individual page.

Apps.js houses the links to each page and is connected to each js file through route paths and imports(at the top of the file). The elements within the routes are used by the nav to form clickable links within the nav bar. These elements are also used within the "home pages"(list pages) provide easier movement through the site.

Each of the List and Form file use jsx which allows for the writing of HTML elements within Javascript and place them in Dom without createElement and/or appendChild methods. JSX as you will notice allows for simpler/cleaner and much more elegant code.

Within each file you will see that useState and useEffect are used. These are hooks which create functional states to components within the code such as
setSales(salesData.sales) which updates the variable sales with the array of sales data received from the API. this is how communication is facilitated from the front end to the backend API.

This project as a whole allows the tracking/creation of auto sales and services. This is a full service dealership allowing for the creation/tracking of inventory, sales, customers and employees both in service and sales.
