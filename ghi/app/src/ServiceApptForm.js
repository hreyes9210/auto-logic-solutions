import React,{ useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function ServiceApptForm() {
    console.log('Rendering ServiceApptForm...');
    const navigate = useNavigate();
    const [technicians, setTechnicians] = useState([]);
    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: '',
});

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technician);
        }
    };

    useEffect(() => {
        console.log('Fetching data...');
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();



        const url = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                date_time: '',
                reason: '',
                vin: '',
                customer: '',
                technician: '',
            });
            navigate('/appointments')
        }
    }
    const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
        ...formData,
        [inputName]: value
    })

}

    return(
        <div>
            <h1>Create a Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
                    <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.date} placeholder="Date" required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
                    <label htmlFor="date_time">Date and Time</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleFormChange} value={formData.technician} name="technician" id="technician" className="form-control">
                    <option value="">Choose a Technician</option>
                    {technicians.map(technician => {
                        return (
                            <option value={technician.id} key={technician.id}>
                                {technician.last_name}, {technician.first_name} (Employee Id: {technician.employee_id})
                            </option>
                        )
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                    <label htmlFor="reason">Reason</label>
                </div>
                <button className="btn btn-primary" type="submit">Create</button>
            </form>
        </div>
    )

}
export default ServiceApptForm;
