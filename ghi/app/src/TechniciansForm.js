import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function TechniciansForm() {
    const navigate = useNavigate();
    const [formData, setFormData] = useState ({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        setFormData({
            first_name: '',
            last_name: '',
            employee_id: '',
        });
        navigate("/technicians");
    }
}
const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
        ...formData,
        [inputName]: value
    })
}

return (
    <div>
        <h1>Add a Technician</h1>
        <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" type="text" required name="first_name" id="first_name" className="form-control"/>
                <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" type="text" required name="last_name" id="last_name" className="form-control"/>
                <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.employee_id} placeholder="Employee Id" type="text" required name="employee_id" id="employee_id" className="form-control"/>
                <label htmlFor="employee_id">Employee Id</label>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
    </div>
);


}
export default TechniciansForm; 
