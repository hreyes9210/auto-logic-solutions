import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([])
  const navigate = useNavigate();

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople)
    }
  }

  useEffect(() => {
    getData()
  }, []);

const deleteSalesperson = async(salespersonId) => {
    const response = await fetch(`http://localhost:8090/api/salespeople/${salespersonId}`, {
        method: "DELETE",
    });

    if (response.ok) {
        setSalespeople(salespeople.filter(salesperson => salesperson.id !== salespersonId));
    } else {
        console.error('failed to delete salesperson');
    }
};

  const navToSales = () => {
    navigate('/sales/')
  };

  const navToNewSalesperson = () => {
    navigate('/salespeople/new')
  };

  const navToSalesPersonHistory = () => {
    navigate('/sales/history')
  };

    return (
        <div
        style={{ marginTop: '30px', marginBottom: '90px' }}
        >
            <table className="table table-striped border border-dark regular shadow">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                            <td>{ salesperson.employee_id }</td>
                            <td>
                                <button
                                    className="btn btn-outline-danger"
                                    onClick={() => deleteSalesperson(salesperson.id)}>
                                        Delete
                                </button>
                            </td>
                        </tr>
                       )
                    })}
            </tbody>
        </table>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToSales}
        >
            Sales Main
        </button>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToNewSalesperson}
        style={{ marginLeft: '10px' }}
        >
            New Sales Person
        </button>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToSalesPersonHistory}
        style={{ marginLeft: '10px' }}
        >
            Sales History
        </button>
        <div style={{ marginBottom: '90px' }}>
        </div>
        </div>

    );
}

export default SalespeopleList
