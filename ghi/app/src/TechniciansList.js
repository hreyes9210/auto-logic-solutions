
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'

function TechniciansList() {
    const [technicianList, setTechnicianList] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians';
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            setTechnicianList(data.technician);
            
        }
    }
    
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h1>Technicians</h1>
            <table className="table table-striped border border-dark">
                <thead>
                    <tr>
                        <th>Employee Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicianList.map(technician => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
           </table>
           <Link to="./new">
                    <button>Add a Technician</button>
                </Link>
        </div>
    );
}
    
    
    
    export default TechniciansList; 