import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function SalesList() {
  const [sales, setSales] = useState([]);
  const navigate = useNavigate();

  const getData = async () => {
    try {
      const salesResponse = await fetch('http://localhost:8090/api/sales/');
      if (salesResponse.ok) {
        const salesData = await salesResponse.json();
        setSales(salesData.sales);

      }

    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const deleteSale = async (saleId) => {
    try {
      const response = await fetch(`http://localhost:8090/api/sales/${saleId}`, {
        method: 'DELETE',
      });

      if (response.ok) {
        setSales(sales.filter((sale) => sale.id !== saleId));
      } else {
        console.error('Failed to delete sale');
      }
    } catch (error) {
      console.error('Error deleting sale:', error);
    }
  };

  const navToNewSale = () => {
    navigate('/sales/new');
  };

  const navToSalesPeople = () => {
    navigate('/salespeople');
  };

  const navToCustomers = () => {
    navigate('/customers');
  };

  return (
    <div style={{ marginTop: '30px' }}>
      <table className="table table-striped border border-dark regular shadow">
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales && sales.map(sale => {
            return (
            <tr key={sale.id}>
              <td>{sale.salesperson.employee_id}</td>
              <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
              <td>
                <button className="btn btn-outline-danger" onClick={() => deleteSale(sale.id)}>
                  Delete
                </button>
              </td>
            </tr>
            )
            })}
        </tbody>
      </table>
      <div style={{ marginTop: '10px' }}>
        <button
          type="button"
          className="btn btn-outline-primary"
          onClick={navToNewSale}
        >
          New Sale
        </button>
        <button
          type="button"
          className="btn btn-outline-primary"
          onClick={navToSalesPeople}
          style={{ marginLeft: '10px' }}
        >
          Sales People
        </button>
        <button
          type="button"
          className="btn btn-outline-primary"
          onClick={navToCustomers}
          style={{ marginLeft: '10px' }}
        >
          Customers
        </button>
      </div>
    </div>
  );
}

export default SalesList;
