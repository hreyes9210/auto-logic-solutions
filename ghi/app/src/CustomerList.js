import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function CustomerList() {
  const [customers, setCustomer] = useState([])
  const navigate = useNavigate();

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/customers/');

    if (response.ok) {
      const data = await response.json();
      setCustomer(data.customer)
    }
  }

  useEffect(() => {
    getData()
  }, []);

const deleteCustomer = async(customerId) => {
    const response = await fetch(`http://localhost:8090/api/customers/${customerId}`, {
        method: "DELETE",
    });

    if (response.ok) {
        setCustomer(customers.filter(customer => customer.id !== customerId));
    } else {
        console.error('failed to delete customer');
    }
};

  const navToSales = () => {
    navigate('/sales/')
  };

  const navToNewCustomer = () => {
    navigate('/customers/new')
  };

    return (
        <div>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                {customers.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td>{ customer.first_name }</td>
                            <td>{ customer.last_name }</td>
                            <td>{ customer.phone_number }</td>
                            <td>{ customer.address }</td>
                            <td>
                                <button
                                    className="btn btn-outline-danger"
                                    onClick={() => deleteCustomer(customer.id)}>
                                        Delete
                                </button>
                            </td>
                        </tr>
                       )
                    })}
            </tbody>
        </table>
        <div style={{ marginTop: '10px' }}>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToSales}
        >
            Sales Main
        </button>
        <button type="button"
        className="btn btn-outline-primary"
        onClick={navToNewCustomer}
        style={{ marginLeft: '10px' }}
        >
            New Customer
        </button>
        </div>
    </div>

    );
}

export default CustomerList
