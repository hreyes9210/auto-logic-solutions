import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechniciansForm from './TechniciansForm';
import TechniciansList from './TechniciansList';
import ServiceApptForm from './ServiceApptForm';
import ServiceList from './ServiceList';
import ServiceHistory from './ServiceHistory';
import CreateModel from './InvCreateModel';
import CreateAutomobile from './InvCreateAutomobile';
import AutomobileList from './InvAutomobileList';
import SalespersonForm from './SalesPersonForm';
import SalespeopleList from './SalesPeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import SalesPersonHistory from './SalesPersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/" element={<TechniciansList />} />
          <Route path="/technicians/new" element={<TechniciansForm/>} />
          <Route path="/appointments" element={<ServiceList/>} />
          <Route path="/appointments/new" element={<ServiceApptForm/>} />
          <Route path="/appointments/history" element={<ServiceHistory/>} />
          <Route path="/models/create" element={<CreateModel/>} />
          <Route path="/automobiles" element={<AutomobileList/>} />
          <Route path="/automobiles/create" element={<CreateAutomobile/>} />
          <Route path="/salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="/salespeople/new" element={<SalespersonForm />} />
          </Route>
          <Route path="/customers">
            <Route index element={<CustomerList />} />
            <Route path="/customers/new" element={<CustomerForm />} />
          </Route>
          <Route path="/sales">
            <Route index element={<SalesList />} />
            <Route path="/sales/new" element={<SalesForm />} />
            <Route path="/sales/history" element={<SalesPersonHistory />} />
          </Route>
          <Route path="/manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          </Route>
          <Route path="/models/" element={<ModelsList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
