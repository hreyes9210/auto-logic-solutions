function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">

      <h1 className="display-5 fw-bold">AutoFlow</h1>
      <img src="https://images.unsplash.com/photo-1559533670-2195d0363733?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OTl8fGNhciUyMGRlYWxlcnNoaXB8ZW58MHwwfDB8fHww" alt="Three highend sports cars" width="600" height="350"/>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
        The unrivaled choice for dealership management begins here!
        </p>
      </div>
    </div>
  );
}

export default MainPage;
