import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function ManufacturerForm() {
  const navigate = useNavigate();
  const [manufacturerData, setManufacturer] = useState({
    name: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(manufacturerData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setManufacturer({
        name: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setManufacturer({
      ...manufacturerData,
      [inputName]: value,
    });
  };

  const navToAutomobiles = () => {
    navigate('/automobiles/')
  };

  const navToManufacturers = () => {
    navigate('/manufacturers/')
  };


  return (
    <div
    className="row"
    style={{ marginTop: '30px', marginBottom: '90px' }}
    >
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                value={manufacturerData.name}
                onChange={handleFormChange}
                placeholder="Manufacturers Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Manufacturer Name</label>
            </div>
            <button className="btn btn-outline-primary d-grid gap-2 col-6 mx-auto"
            >
              Create
            </button>
          </form>
        </div>
        <div style={{ marginTop: '10px' }}>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToAutomobiles}
        >
            Inventory
        </button>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToManufacturers}
        style={{ marginLeft: '10px' }}
        >
            Manufacturers
        </button>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
