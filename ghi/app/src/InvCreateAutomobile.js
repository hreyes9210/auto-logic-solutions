import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateAutomobile () {
    const navigate = useNavigate();
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState ({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    })

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
            color: '',
            year: '',
            vin: '',
            model_id: '',
            });
            navigate('/automobiles');
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const navToAutomobiles = () => {
      navigate('/automobiles/')
    };



    return (
        <div
        className="row"
        style={{ marginTop: '30px', marginBottom: '90px' }}
        >
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an Automobile to Inventory</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.year} placeholder="Year" required type="text" name="year" id="year" className="form-control"/>
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                  <option value="">Choose a Model</option>
                    {models.map(model => {
                        return (
                            <option value={model.id} key={model.id}>
                                {model.name}
                            </option>
                        )
                    })}

                </select>
              </div>
              <button
              className="btn btn-outline-primary d-grid gap-2 col-6 mx-auto"
              >
                Create
                </button>
            </form>
          </div>
          <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToAutomobiles}
        style={{ marginTop: '10px' }}
        >
            Inventory
        </button>
        </div>
      </div>
    )
    }




export default CreateAutomobile;
