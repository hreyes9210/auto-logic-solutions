import { NavLink } from 'react-router-dom';
import { NavDropdown, Nav, Navbar } from 'react-bootstrap';
import { useState } from 'react';

function MyNavbar() {

  const [expanded, setExpanded] = useState(false);

  const closeNavbar = () => {
    setExpanded(false);
  };

  return (
    <Navbar expand="lg" variant="dark" bg="info" expanded={expanded}>
      <div className="container-fluid">
        <Navbar.Brand as={NavLink} to="/" onClick={closeNavbar}>
          AutoFlow
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="navbarSupportedContent"
          onClick={() => setExpanded(!expanded)}
        />
        <Navbar.Collapse id="navbarSupportedContent">
          <Nav className="mr-auto">
            <NavDropdown title="Sales Drop Down" id="salesDropdown" onSelect={closeNavbar}>
              <NavDropdown.Item as={NavLink} to="/sales">
                Sales
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/sales/new">
                Create a New Sale
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/salespeople">
                Sales People
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/salespeople/new">
                Add a Sales Person
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/salespeople/history">
                Sales Person History
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/customers">
                Customers
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/customers/new">
                Create a New Customers
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Inventory Drop Down" id="inventoryDropdown" onSelect={closeNavbar}>
              <NavDropdown.Item as={NavLink} to="/automobiles">
                Inventory
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/automobiles/create">
                Create An automobile
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/manufacturers">
                Manufacturers
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/manufacturers/new">
                Create a New Manufacturers
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/models">
                Models
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/models/create">
                Create a Model
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Service Dropdown" id="serviceDropdown" onSelect={closeNavbar}>
              <NavDropdown.Item as={NavLink} to="/appointments">
                Service appointments
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/appointments/new">
                Create a Service Appointment
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/appointments/history">
                Service History
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/technicians">
                Technicians
              </NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/technicians/new">
                Add a Technician
              </NavDropdown.Item>
            </NavDropdown>
            </Nav>
        </Navbar.Collapse>
      </div>
    </Navbar>
  );
}

export default MyNavbar;
