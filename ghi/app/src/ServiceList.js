import React, { useEffect, useState } from "react";

function ServiceList() {
    const [serviceList, setServiceList] = useState([]);
    const [vehicleDetails, setVehicleDetails] = useState({});

    const formatDate = (dateString) => {
        const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        return new Date(dateString).toLocaleDateString(undefined, options);
    };

    const formatTime = (timeString) => {
        const options = { hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: true };
        return new Date(timeString).toLocaleTimeString(undefined, options);
    };


    const fetchVehicleDetails = async (vin) => {
        try {
          const url = `http://localhost:8100/api/automobiles/${vin}`;
          const response = await fetch(url);

          if (response.ok) {
            const data = await response.json();
            setVehicleDetails((prevDetails) => ({
              ...prevDetails,
              [vin]: data,
            }));
          } else if (response.status === 404) {
            console.log(`VIN ${vin} does not match anything in inventory`);
          } else {
            console.error(`Error fetching vehicle details for VIN ${vin}:`, response.status);
          }
        } catch (error) {
          console.error(`Error fetching vehicle details for VIN ${vin}:`, error);
        }
      };



        const fetchData = async () => {
          try {
            const url = 'http://localhost:8080/api/appointments/';
            const response = await fetch(url);

            if (response.ok) {
              const data = await response.json();
              const filteredServiceList = data.appointment.filter(service => service.status === 'created');
              for (const service of filteredServiceList) {
                await fetchVehicleDetails(service.vin);
              }

              setServiceList(filteredServiceList);

            }
          } catch (error) {
            console.error('Error fetching service appointments:', error);
          }
        };

    useEffect(() => {
        fetchData();
      }, []);

    const handleCancelButtonClick = async (serviceId) => {

        const url = `http://localhost:8080/api/appointments/${serviceId}/cancel/`;

        const fetchConfig = {
            method: 'put',
            body: JSON.stringify({ "status": "cancelled" }),
            headers: {
                'Content-Type': 'application/json',
            },
         };
         try {
            const response = await fetch(url, fetchConfig);

            if (response.ok) {
              setServiceList((prevServiceList) => prevServiceList.filter((service) => service.id !== serviceId));
            }
          } catch (error) {
            console.error(`Error cancelling service appointment ${serviceId}:`, error);
          }
        };



    const handleFinishButtonClick = async (serviceId) => {

        const url = `http://localhost:8080/api/appointments/${serviceId}/finish/`;

        const fetchConfig = {
            method: 'put',
            body: JSON.stringify({ "status": "finished" }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(url, fetchConfig);

            if (response.ok) {
              setServiceList((prevServiceList) => prevServiceList.filter((service) => service.id !== serviceId));
            }
          } catch (error) {
            console.error(`Error finishing service appointment ${serviceId}:`, error);
          }
        };

    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped border border-dark">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {serviceList.map(service => {
                        const isVIP = vehicleDetails[service.vin] || false;
                        return (
                            <tr key={service.id}>
                                <td>{service.vin}</td>
                                <td>{isVIP ? 'Yes' : 'No'}</td>
                                <td>{service.customer}</td>
                                <td>{formatDate(service.date_time)}</td>
                                <td>{formatTime(service.date_time)}</td>
                                <td>{service.technician.first_name} {service.technician.last_name}</td>
                                <td>{service.reason}</td>
                                <td>
                                    <button
                                        className="btn btn-danger btn-rounded"
                                        onClick={() => handleCancelButtonClick(service.id)}
                                    >
                                        Cancel
                                    </button>
                                    <button
                                        className="btn btn-success btn-rounded"
                                        onClick={() => handleFinishButtonClick(service.id)}
                                    >
                                        Finish
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ServiceList;
