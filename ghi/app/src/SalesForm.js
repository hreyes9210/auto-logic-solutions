import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom'

function SalesForm() {
    const [sale, setSale] = useState({
      automobile: 'vin',
      salesperson: 'id',
      customer: 'id',
      price: '',
    });
    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const navigate = useNavigate();

    const getData = async () => {
        try {
          const automobilesResponse = await fetch('http://localhost:8100/api/automobiles/');
          if (automobilesResponse.ok) {
            const automobilesData = await automobilesResponse.json();
            setAutomobiles(automobilesData.autos);
          }

          const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
          if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople);
          }

          const customersResponse = await fetch('http://localhost:8090/api/customers/');
          if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            setCustomers(customersData.customer);
          }
        } catch (error) {
          console.error('Error fetching data:', error);
        }
      };

      useEffect(() => {
        getData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();
        const salesUrl = 'http://localhost:8090/api/sales/';
        const automobileUrl = `http://localhost:8100/api/automobiles/${sale.automobile}/`;  // Use the selected automobile VIN

        const fetchSaleConfig = {
          method: "post",
          body: JSON.stringify(sale),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        try {
          const saleResponse = await fetch(salesUrl, fetchSaleConfig);
          if (!saleResponse.ok) {
            throw new Error('Failed to create sale');
          }

          const automobileUpdateConfig = {
            method: "put",
            body: JSON.stringify({ sold: true }),
            headers: {
              'Content-Type': 'application/json',
            },
          };

          const automobileUpdateResponse = await fetch(automobileUrl, automobileUpdateConfig);
          if (!automobileUpdateResponse.ok) {
            throw new Error('Failed to update automobile status');
          }

          setSale({
            automobile: 'vin',
            salesperson: 'id',
            customer: 'id',
            price: '',
          });

          console.log('Sale and automobile update successful');
        } catch (error) {
          console.error('Error:', error);
        }
      };

    const handleFormChange = (e) => {
      const value = e.target.value;
      const inputName = e.target.name;
      setSale({
        ...sale,
        [inputName]: value,
      });
    };

      const navToSale = () => {
        navigate('/sales/');
      };

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select
                value={sale.automobile}
                onChange={handleFormChange}
                required
                name="automobile"
                id="automobile"
                className="form-select"
              >
                <option value="">Select an Automobile VIN</option>
                    {automobiles.map((automobile) => {
                        return (
                        <option key={automobile.vin} value={automobile.vin}>
                            {automobile.vin}
                        </option>
                        );
                    })}
              </select>
            </div>
            <div className="mb-3">
              <select
                value={sale.salesperson}
                onChange={handleFormChange}
                required
                name="salesperson"
                id="salesperson"
                className="form-select"
              >
                <option value="">Select a Salesperson</option>
                    {salespeople.map((salesperson) => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>
                                {salesperson.first_name}, {salesperson.last_name}
                            </option>
                        );
                    })}
              </select>
            </div>
            <div className="mb-3">
              <select
                value={sale.customer}
                onChange={handleFormChange}
                required
                name="customer"
                id="customer"
                className="form-select"
              >
                <option value="">Select a Customer</option>
                    {customers.map((customer) => {
                        return (
                            <option key={customer.id} value={customer.id}>
                                {customer.first_name}, {customer.last_name}
                            </option>
                        );
                    })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                value={sale.price}
                onChange={handleFormChange}
                placeholder="Price"
                required
                type="number"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
              <button className="btn btn-outline-primary d-grid gap-2 col-6 mx-auto">Create Sale</button>
            </form>
          </div>
          <div style={{ marginTop: '10px' }}>
          <button
            type="button"
            className="btn btn-outline-primary"
            onClick={navToSale}
            >
            Sales Main
            </button>
          </div>
        </div>
      </div>
    );
  }

  export default SalesForm;
