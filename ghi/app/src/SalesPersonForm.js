import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SalespersonForm() {
  const [salespeopleData, setSalesperson] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(salespeopleData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      setSalesperson({
        first_name: '',
        last_name: '',
        employee_id: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setSalesperson({
      ...salespeopleData,
      [inputName]: value,
    });
  };

  const navToSales = () => {
    navigate('/sales/');
  };

  const navToSalesPeople = () => {
    navigate('/salespeople/');
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Sales Person</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input
                value={salespeopleData.first_name}
                onChange={handleFormChange}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={salespeopleData.last_name}
                onChange={handleFormChange}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
              />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={salespeopleData.employee_id}
                onChange={handleFormChange}
                placeholder="Employee ID"
                required
                type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <div className="d-grid gap-2 col-6 mx-auto">
            <button className="btn btn-outline-primary">Create</button>
            </div>
          </form>
        </div>
        <div style={{ marginTop: '10px' }}>
          <button
            type="button"
            className="btn btn-outline-primary"
            onClick={navToSales}
            >
              Sales Main
          </button>
          <button
            type="button"
            className="btn btn-outline-primary"
            onClick={navToSalesPeople}
            style={{ marginLeft: '10px' }}
            >
            Sales People
            </button>
        </div>
      </div>
    </div>
  );
}

export default SalespersonForm;
