import React, { useEffect, useState } from "react";

function ServiceHistory() {
    const [serviceList, setServiceList] = useState([]);
    const [vehicleDetails, setVehicleDetails] = useState({});
    const [filterValue, setFilterValue] = useState("");

    const formatDate = (dateString) => {
        const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        return new Date(dateString).toLocaleDateString(undefined, options);
    };

    const formatTime = (timeString) => {
        const options = { hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: true };
        return new Date(timeString).toLocaleTimeString(undefined, options);
    };


    const fetchVehicleDetails = async (vin) => {
        try {
          const url = `http://localhost:8100/api/automobiles/${vin}`;
          const response = await fetch(url);

          if (response.ok) {
            const data = await response.json();
            setVehicleDetails((prevDetails) => ({
              ...prevDetails,
              [vin]: data,
            }));
          } else if (response.status === 404) {
            console.log(`VIN ${vin} does not match anything in inventory`);
          } else {
            console.error(`Error fetching vehicle details for VIN ${vin}:`, response.status);
          }
        } catch (error) {
          console.error(`Error fetching vehicle details for VIN ${vin}:`);
        }
      };


    const fetchData = async () => {
        try {
            const url = 'http://localhost:8080/api/appointments/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                const vehicleDetailsPromises = data.appointment.map(service => fetchVehicleDetails(service.vin));

                const vehicleDetailsArray = await Promise.all(vehicleDetailsPromises);

                const combinedData = data.appointment.map((service, index) => ({
                  ...service,
                  isVIP: vehicleDetailsArray[index]?.is_vip || false,
                }));

                setServiceList(combinedData);
                return data;
              }
            } catch (error) {
              console.error('Error during fetch:', error);
              throw error;
            }
          };

    useEffect(() => {
        fetchData();
    }, []);

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        try {
            const data = await fetchData();
        } catch (error) {
            console.error('Error during fetch:', error);
        }
    };

    function handleFilterChange(e) {
      setFilterValue(e.target.value)
    }

    return (
        <div>
            <h1>Service History</h1>
            <form onSubmit={handleFormSubmit}>
                <div className="form-floating mb-3">
                    <input onChange={handleFilterChange}className="form-control"/>
                    <label >Search by VIN...</label>
                    <button type="submit">Search</button>
                </div>
            </form>
            <table className="table table-striped border border-dark">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {serviceList
                    .filter((serviceList) =>
                    serviceList.vin.includes(filterValue)
                    )
                    .map(service => {
                        const isVIP = vehicleDetails[service.vin] || false;
                        return (
                        <tr key={service.id}>
                            <td>{service.vin}</td>
                            <td>{isVIP ? 'Yes' : 'No'}</td>
                            <td>{service.customer}</td>
                            <td>{formatDate(service.date_time)}</td>
                            <td>{formatTime(service.date_time)}</td>
                            <td>{service.technician.first_name} {service.technician.last_name}</td>
                            <td>{service.reason}</td>
                            <td>{service.status}</td>
                        </tr>
                        )
                })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
