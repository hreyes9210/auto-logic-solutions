import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateModel () {
    const navigate = useNavigate();
    const [manufactures, setManufactures] = useState([]);
    const [formData, setFormData] = useState ({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManufactures(data.manufacturers);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
            name: '',
            picture_url: '',
            manufacturer_id: '',
            });
            navigate('/models');
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const navToAutomobiles = () => {
      navigate('/automobiles/')
    };

    const navToModels = () => {
      navigate('/models/')
    };

    return (
        <div
        className="row"
        style={{ marginTop: '30px', marginBottom: '90px' }}
        >
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Vehicle Model</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name} placeholder="Model Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.picture_url} placeholder="URL of Picture" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} value={formData.manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                  <option value="">Choose A Manufacturer</option>
                    {manufactures.map(manufacture => {
                        return (
                            <option value={manufacture.id} key={manufacture.id}>
                                {manufacture.name}
                            </option>
                        )
                    })}

                </select>
              </div>
              <button
              className="btn btn-outline-primary d-grid gap-2 col-6 mx-auto"
              >
                Create
              </button>
            </form>
          </div>
          <div style={{ marginTop: '10px' }}>
          <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToAutomobiles}
        >
            Inventory
        </button>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToModels}
        style={{ marginLeft: '10px' }}
        >
            Models
        </button>
        </div>
        </div>
      </div>
    )
    }




export default CreateModel;
