import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([])
  const navigate = useNavigate();

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    getData()
  }, []);

const deleteManufacturer = async(manufacturersId) => {
    const response = await fetch(`http://localhost:8100/api/manufacturers/${manufacturersId}`, {
        method: "DELETE",
    });

    if (response.ok) {
        setManufacturers(manufacturers.filter(manufacturer => manufacturer.id !== manufacturersId));
    } else {
        console.error('failed to delete manufacturer');
    }
};

  const navToAutomobiles = () => {
    navigate('/automobiles/')
  };

  const navToNewManufacturer = () => {
    navigate('/manufacturers/new')
  };

    return (
        <div style={{ marginTop: '30px', marginBottom: '90px' }}>
            <table className="table table-striped border border-dark regular shadow">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{ manufacturer.name }</td>
                            <td>
                                <button
                                    className="btn btn-outline-danger"
                                    onClick={() => deleteManufacturer(manufacturer.id)}>
                                        Delete
                                </button>
                            </td>
                        </tr>
                       )
                    })}
            </tbody>
        </table>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToAutomobiles}
        >
            Inventory
        </button>
        <button type="button"
        className="btn btn-outline-primary"
        onClick={navToNewManufacturer}
        style={{ marginLeft: '10px' }}
        >
            New Manufacturer
        </button>
        <div style={{ marginBottom: '90px' }}>
        </div>
        </div>

    );
}

export default ManufacturersList
