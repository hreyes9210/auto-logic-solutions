import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function SalesPersonHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const navigate = useNavigate();

  const getData = async () => {
    try {
      const salesResponse = await fetch('http://localhost:8090/api/sales/');
      const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
      if (salesResponse.ok && salespeopleResponse.ok) {
        const salesData = await salesResponse.json();
        const salespeopleData = await salespeopleResponse.json();
        setSales(salesData.sales);
        setSalespeople(salespeopleData.salespeople);
        setFilteredSales(salesData.sales);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSalespersonChange = (e) => {
    const selectedSalespersonId = e.target.value;
    setSelectedSalesperson(selectedSalespersonId);
    filterSalesBySalesperson(selectedSalespersonId);
  };

  const filterSalesBySalesperson = (salespersonId) => {
    if (salespersonId === '') {
      setFilteredSales(sales);
    } else {
      const filtered = sales.filter((sale) => sale.salesperson.id.toString() === salespersonId);
      setFilteredSales(filtered);
    }
  };

 const navToSales = () => {
    navigate('/sales/');
  };

  const navToSalespeople = () => {
    navigate('/salespeople/');
  };
  return (
    <div style={{ marginTop: '30px', marginBottom: '90px' }}>
      <div style={{ marginTop: '30px' }}>
        <label htmlFor="salespersonFilter">Filter by Salesperson:</label>
        <select
          id="salespersonFilter"
          onChange={handleSalespersonChange}
          value={selectedSalesperson}
        >
          <option value="">All Salespeople</option>
          {salespeople.map((salesperson) => (
            <option key={salesperson.id} value={salesperson.id}>
              {salesperson.id}
            </option>
          ))}
        </select>
      </div>
      <table style={{ marginTop: '20px' }} className="table table-striped border border-dark regular shadow">
        <thead>
          <tr>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {filteredSales.map(sale => (
            <tr key={sale.id}>
              <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <div style={{ marginTop: '10px' }}>
        <button
          type="button"
          className="btn btn-outline-primary"
          onClick={navToSales}
        >
        Sales Main
        </button>
        <button
          type="button"
          className="btn btn-outline-primary"
          onClick={navToSalespeople}
          style={{ marginLeft: '10px' }}
        >
        Sales People
        </button>
      </div>
    </div>
  );
}

export default SalesPersonHistory;
