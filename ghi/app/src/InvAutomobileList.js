
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'

function AutomobileList() {
    const [automobileList, setAutomobileList] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobileList(data.autos);

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div style={{ marginTop: '30px', marginBottom: '90px' }}>
            <h1
            style={{ marginBottom: '20px'}}
            >Inventory</h1>
            <table className="table table-striped border border-dark regular shadow">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobileList.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{String(auto.sold)}</td>
                                <td><img src={auto.model.picture_url} className="img-thumbnail Regular shadow" alt="car" style={{height: '170px', maxWidth:'170px', minWidth:'130px'}}/></td>
                            </tr>
                        )
                    })}
                </tbody>
           </table>
           <Link
           to="./create">
                    <button
                    type="button"
                    className="btn btn-outline-primary"
                    >
                        Add Automobile
                    </button>
          </Link>
          <Link
           to="/manufacturers">
                    <button
                    type="button"
                    className="btn btn-outline-primary"
                    style={{ marginLeft: '10px' }}
                    >
                        Manufacturers
                    </button>
          </Link>
          <Link
           to="/models">
                    <button
                    type="button"
                    className="btn btn-outline-primary"
                    style={{ marginLeft: '10px' }}
                    >
                        Models
                    </button>
          </Link>
        </div>
    );
}



    export default AutomobileList;
