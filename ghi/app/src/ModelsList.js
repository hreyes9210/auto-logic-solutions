import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function ModelsList() {
  const [models, setModels] = useState([])
  const navigate = useNavigate();

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');

    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
    }
  }

  useEffect(() => {
    getData()
  }, []);

const deleteModel = async(modelsId) => {
    const response = await fetch(`http://localhost:8100/api/models/${modelsId}`, {
        method: "DELETE",
    });

    if (response.ok) {
        setModels(models.filter(model => model.id !== modelsId));
    } else {
        console.error('failed to delete model');
    }
};

const navToAutomobiles = () => {
    navigate('/automobiles/')
  };

const navToNewModel = () => {
    navigate('/models/create')
  };

    return (
        <div style={{ marginTop: '30px' }}>
            <table className="table table-striped border border-dark regular shadow">
            <thead>
                <tr>
                    <th>Models</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                        <tr key={model.id}>
                            <td>{ model.name }</td>
                            <td>{ model.manufacturer.name }</td>
                            <td><img src={ model.picture_url } className="img-thumbnail" alt="car" style={{height: '170px', maxWidth: '170px', minWidth:'130px'}}/></td>
                            <td>
                                <button
                                    className="btn btn-outline-danger"
                                    onClick={() => deleteModel(model.id)}>
                                        Delete
                                </button>
                            </td>
                        </tr>
                       )
                    })}
            </tbody>
        </table>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToAutomobiles}
        >
            Inventory
        </button>
        <button
        type="button"
        className="btn btn-outline-primary"
        onClick={navToNewModel}
        style={{ marginLeft: '10px' }}
        >
            Add Model
        </button>
        <div style={{ marginBottom: '90px' }}>
        </div>
        </div>

    );
}

export default ModelsList
