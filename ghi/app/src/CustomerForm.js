import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CustomerForm() {
  const navigate = useNavigate();
  const [customers, setCustomer] = useState({
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(customers),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      setCustomer({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setCustomer({
      ...customers,
      [inputName]: value,
    });
  };

  const navToSales = () => {
    navigate('/sales/')
  };

  const navToCustomers = () => {
    navigate('/customers');
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input
                value={customers.first_name}
                onChange={handleFormChange}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={customers.last_name}
                onChange={handleFormChange}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
              />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={customers.address}
                onChange={handleFormChange}
                placeholder="Address"
                required
                type="text"
                name="address"
                id="address"
                className="form-control"
              />
              <label htmlFor="address">Address</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={customers.phone_number}
                onChange={handleFormChange}
                placeholder="Phone Number"
                required
                type="text"
                name="phone_number"
                id="phone_number"
                className="form-control"
              />
              <label htmlFor="phone_number">Phone Number</label>
            </div>
            <div className="d-grid gap-2 col-6 mx-auto">
            <button
            className="btn btn-outline-primary"
            >
              Create Customer
            </button>
            </div>
            </form>
            </div>
            <div style={{ marginTop: '10px' }}>
            <button
            type="button"
            className="btn btn-outline-primary"
            onClick={navToSales}
            >
                Sales Main
            </button>
            <button
              type="button"
              className="btn btn-outline-primary"
              onClick={navToCustomers}
              style={{ marginLeft: '10px' }}
            >
              Customers
          </button>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
