from django.contrib import admin
from .models import AutomobileVO, Sale, Customer, Salesperson
# Register your models here.
@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = (
        'pk', '__str__'
    )

@admin.register(Sale)
class SalesAdmin(admin.ModelAdmin):
    list_display = (
        'pk', '__str__'
    )

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'pk', '__str__'
    )

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = (
        'pk', '__str__'
    )
