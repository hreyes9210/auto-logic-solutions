from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder
import json
from decimal import Decimal


class SalesPersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class SalesPersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

@require_http_methods(["GET", "POST"])
def list_salespeoples(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalesPersonListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonListEncoder,
                safe=False,
                status=200,
            )
        except Exception as e:
            return JsonResponse(
                {"message": f"Invalid Salesperson: {str(e)}"},
                status=400,
            )
    else:
        return JsonResponse(
            {"message": "Method not allowed"},
            status=404,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_salespeople(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDetailEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDetailEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400
            )
    else:
        content = json.loads(request.body)
        try:
            if "salesperson" in content:
                salesperson = Salesperson.objects.get(id=pk)
                content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales Person not on record."},
                status=404,
            )

        Salesperson.objects.filter(id=pk).update(**content)
        updated_saleperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            updated_saleperson,
            encoder=SalesPersonDetailEncoder,
            safe=False,
        )

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
                status=200,
            )
        except Exception as e:
            return JsonResponse(
                {"message": f"Invalid Customer: {str(e)}"},
                status=400,
            )
    else:
        return JsonResponse(
            {"message": "Method not allowed"},
            status=404,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_customers(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                {"message": "Customer deleted successfully."},
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not on file"},
                status=404
                )
    else:
        content = json.loads(request.body)
        try:
            if "customer" in content:
                customer = Customer.objects.get(id=pk)
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not on file."},
                status=404,
            )

        Customer.objects.filter(id=pk).update(**content)
        updated_customer = Customer.objects.get(id=pk)
        return JsonResponse(
            updated_customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


class PriceEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)
        return super(PriceEncoder, self).default(o)


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]


class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "salesperson",
        "customer",
        "automobile",
        "price"
    ]
    encoders = {
    "salesperson": SalesPersonListEncoder(),
    "customer": CustomerListEncoder(),
    "automobile": AutomobileVOEncoder(),
    "price": PriceEncoder(),
    }



class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "salesperson",
        "customer",
        "price"
    ]
    encoders = {
        "salesperson": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),
    }
    def get_extra_data(self, o):
        return {"vin": o.automobile.vin}


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])

            if automobile.sold:
                return JsonResponse({"message": "Automobile has already been sold."}, status=400)
            content["automobile"] = automobile

            salesperson_identifier = content.get("salesperson")
            salesperson = Salesperson.objects.get(id=salesperson_identifier)

            customer_id = content.get("customer")
            customer = Customer.objects.get(id=customer_id)

            price = Decimal(content.get("price", 0))

            content["salesperson"] = salesperson
            content["customer"] = customer
            content["price"] = price

            sale = Sale.objects.create(**content)
            automobile.sold=True
            automobile.save()
            return JsonResponse(
                sale,
                encoder=SaleListEncoder,
                safe=False,
                )

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid automobile"},
                status=404,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid salesperson"},
                status=404,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid customer"},
                status=404,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_sales(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                {"message": "Sale deleted successfully."},
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)
    else:
        content = json.loads(request.body)
        try:
            if "sale" in content:
                sale = Sale.objects.get(id=pk)
                content["sale"] = sale
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not on file."},
                status=400,
            )

        Sale.objects.filter(id=pk).update(**content)
        updated_sale = Sale.objects.get(id=pk)
        return JsonResponse(
            updated_sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )
