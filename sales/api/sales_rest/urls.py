from django.urls import path
from .views import (
    list_salespeoples,
    show_salespeople,
    list_customers,
    show_customers,
    list_sales,
    show_sales,
    # salesperson_history,
    )


urlpatterns = [
    path("salespeople/", list_salespeoples, name="list_salespeoples"),
    path("salespeople/<int:pk>/", show_salespeople, name="show_salespeople"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", show_customers, name="show_customers"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", show_sales, name="show_sales"),
]
